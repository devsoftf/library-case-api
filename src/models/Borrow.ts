import { IsNotEmpty } from 'class-validator';
import { Column, Entity, JoinColumn, PrimaryColumn, OneToOne, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, ManyToMany, ManyToOne } from 'typeorm';
import User from "./User";
import Book from "./Book";

@Entity()
class Borrow{
    @PrimaryGeneratedColumn()
    public borrowId: number;
    
    @Column({
        length:"30"
    })
    public status: string; // waiting or returned

    @ManyToOne(type => User)
    @JoinColumn({name: 'userId'})
    public user: User;

    @ManyToOne(type => Book)
    @JoinColumn({name: 'bookId'})
    public book: Book;

    @Column()
    public score: number;

    @CreateDateColumn()
    public createdAt: string;

    @UpdateDateColumn()
    public updatedAt: string;
}

export default Borrow;