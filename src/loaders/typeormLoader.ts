import "reflect-metadata";
import { createConnection, getConnectionOptions, getConnectionManager, ConnectionManager, useContainer} from 'typeorm';
import { Container } from "typedi";
import ormconfig = require("../../ormconfig");

export const createDbConn = async () => {
    console.log("DB'ye bağlantı kuruluyor...");
    return await createConnection(await getConnectionOptions()).catch(() => {
        console.log("DB'ye bağlanılamadı!");
        console.log("5 saniye sonra bağlantı tekrar'dan başlatılacak.");
        setTimeout(function(){
            createDbConn();
        },5000);
    });
}

export default async function loader(): Promise<any>{
    // setup container
    useContainer(Container);

    // connect to db
    await createDbConn();
}