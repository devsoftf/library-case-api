import * as express from "express";
import * as morgan from "morgan";
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';

@Middleware({ type: 'before' })
export default class LogMiddleware implements ExpressMiddlewareInterface{

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        return morgan("combined")(req,res,next);
    }

}