import { OrmRepository } from 'typeorm-typedi-extensions';
import BookRepository from "../repositories/BookRepository";
import Book from "../models/Book";
import { Service } from 'typedi';


@Service()
export default class BookService{
    constructor(
        @OrmRepository() private bookRepository: BookRepository
    ){}

    public async createBook(book: Book): Promise<Book>{
       return await this.bookRepository.save(book);
    }

    public findBooks(): Promise<Book[]>{
        return this.bookRepository.find();
    }

    public findBookById(bookId: number): Promise<Book>{
        return this.bookRepository.findOne({bookId});
    }
}