import { IsNotEmpty, MaxLength} from 'class-validator';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
class Book{
    @PrimaryGeneratedColumn()
    public bookId: number;

    @IsNotEmpty()
    @Column()
    public bookName: string;

    @CreateDateColumn()
    public createdAt: string;

    @UpdateDateColumn()
    public updatedAt: string;
}

export default Book;