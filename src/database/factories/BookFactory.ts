import * as Faker from 'faker';
import { define } from 'typeorm-seeding';

import Book from '../../models/Book';

define(Book, (faker: typeof Faker) => {
    const book = new Book();
    book.bookName = Faker.name.findName(); // generate new book name

    return book;
});