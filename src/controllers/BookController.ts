import {JsonController, Get, Post, Body, HttpError, Param} from "routing-controllers";
import BookService from "../services/BookService";
import Book from "../models/Book";
import {MinLength, MaxLength, validateOrReject, IsNotEmpty} from "class-validator";
import Borrow from "../models/Borrow";
import BorrowService from "../services/BorrowService";

export class CreateBookBody{

    @IsNotEmpty({
        message:"Kitap adı alanı boş bırakılamaz."
    })
    @MinLength(2,{
        message:"Kitap adı minumum 2 karakterden oluşmalı."
    })
    @MaxLength(225,{
        message:"Kitap adı maksimum 225 karakterden oluşabilir."
    })
    public name: string;

}

export class BookResponse{
    public id: number;
    public name: string;
}

export class BookNotFoundError extends HttpError{
    constructor(){
        super(500,"Kitap bulunamadı.");
    }
}

@JsonController('/books')
export default class BookController{
    constructor(
        private readonly bookService: BookService,
        private readonly borrowService: BorrowService
    ){}
    
    @Get()
    public async list(): Promise<BookResponse[]>{
        var bookList: Book[] = await this.bookService.findBooks();

        var response: BookResponse[] = bookList.map((book) => {return {id: book.bookId, name: book.bookName}});
        
        return response;
    }

    @Get("/:bookId")
    public async detail(@Param('bookId') bookId: number){
        var book: Book = await this.bookService.findBookById(bookId);
        // kitap yoksa hata fırlatır
        if(!book){
            throw new BookNotFoundError();
        }

        var avgScore:any = await this.borrowService.getAvgScoreOfBook(book.bookId);

        return {
            id: book.bookId,
            name: book.bookName,
            score:parseInt(avgScore.avg ? avgScore.avg : 0)
        }
    }
    
    @Post()
    public async create(@Body({required: true}) body: any): Promise<any>{
        // set body param
        var bookBody = new CreateBookBody();
        bookBody.name = body.name;

        // validate request body
        await validateOrReject(bookBody);
        
        const book = new Book();
        book.bookName = bookBody.name;
        
        await this.bookService.createBook(book);

        return {
            message:"Kitap oluşturuldu.",
            data:{
                id: book.bookId,
                name: book.bookName
            }
        };
    }
}