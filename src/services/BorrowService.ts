import { OrmRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';
import Borrow from '../models/Borrow';
import BorrowRepository from '../repositories/BorrowRepository';


@Service()
export default class BorrowService{
    constructor(
        @OrmRepository() private borrowRepository: BorrowRepository
    ){}

    public async findBorrowsByUserIdAndStatus(userId: number, status: string): Promise<Borrow[]>{
        return this.borrowRepository.findBorrowsByUserIdAndStatus(userId,status);
    }

    public createBorrow(borrow: Borrow): Promise<Borrow>{
        return this.borrowRepository.save(borrow);
    }

    public findWaitingBorrow(userId: number, bookId: number): Promise<Borrow>{
        return this.borrowRepository.findOne({
            where:{
                user:{
                    userId
                },
                book:{
                    bookId
                },
                status:"waiting"
            }
        });
    }

    public async updateBorrow(borrow: Borrow): Promise<Borrow>{
        return await this.borrowRepository.save(borrow);
    }

    public async getAvgScoreOfBook(bookId: number){
        return await this.borrowRepository.getAvgScoreOfBook(bookId);
    }
}