import { Application } from 'express';
import { createExpressServer } from 'routing-controllers';
import { Container } from "typedi";
import { useContainer } from 'routing-controllers';
import * as dotenv from "dotenv";
import typeormLoader from "./loaders/typeormLoader";

// set .env file
dotenv.config()

// set typedi container
useContainer(Container);

// load db
typeormLoader();

const expressApp: Application = createExpressServer({
    cors: true,
    classTransformer: true,
    defaultErrorHandler: false,
    controllers: [__dirname+"/controllers/*.js"],
    middlewares: [__dirname+"/middlewares/*.js"]
});

const appPort = 3000;

expressApp.listen(appPort,() => { console.log(`Server started on port: ${appPort}`) });