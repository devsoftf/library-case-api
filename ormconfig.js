const dotenv = require("dotenv");

dotenv.config();

module.exports = {
   "name":"default",
   "type": process.env.DB_TYPE,
   "host": process.env.DB_HOST,
   "port": process.env.DB_PORT,
   "username": process.env.DB_USERNAME,
   "password": process.env.DB_PASS,
   "database": process.env.DB_NAME,
   "synchronize": true,
   "logging": true,
   "connectTimeout":2147483647,
   "entities": [
      __dirname+"/build/src/models/**/*.js"
   ],
   "migrations": [
      __dirname+"/build/src/database/migrations/**/*.js"
   ],
   "subscribers": [],
   "cli": {
      "entitiesDir": __dirname+"/build/src/models",
      "migrationsDir": __dirname+"/build/src/database/migrations",
      "subscribersDir": ""
   }
};