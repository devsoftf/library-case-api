import {Middleware, ExpressErrorMiddlewareInterface, HttpError} from "routing-controllers";
import { ValidationError } from "class-validator";

@Middleware({ type: "after" })
export default class ErrorHandler implements ExpressErrorMiddlewareInterface {

    error(error: any, request: any, response: any, next: (err: any) => any) {
        console.log(error)
        if(error instanceof HttpError){
            response.status(error.httpCode).json({
                message:error.message
            });
        }else if(error[0] && error[0] instanceof ValidationError){
            var validationError:ValidationError = error[0];

            response.status(400).json({
                message:validationError.constraints
            });
        }else{
            response.status(500).json({
                message:"Oopps! Something went wrong!"
            });
        }
    }
    
}