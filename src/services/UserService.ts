import { OrmRepository } from 'typeorm-typedi-extensions';
import { Service } from 'typedi';
import UserRepository from '../repositories/UserRepository';
import User from '../models/User';

@Service()
export default class UserService{
    constructor(
        @OrmRepository() private userRepository: UserRepository
    ){}

    public async createUser(user: User): Promise<User>{
       return this.userRepository.save(user);
    }

    public findUsers(): Promise<User[]>{
        return this.userRepository.find();
    }

    public findUserById(userId: number): Promise<User>{
        return this.userRepository.findOne({userId});
    }
}