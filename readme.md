# Uygulamayı Çalıştırma

Uygulamayı çalıştırmak için, bilgisayarınızda Docker kurulu olması gerekmektedir. 
Docker bulunuyorsa, aşağıdaki komutu çalıştırın.

```console
    foo@bar:~$ docker-compose up
```


Eğer, bu yöntemi uygulamak istemiyorsanız;

MySQL'den 'library_case' adında bir database açın.

Projenin içerisindeki .env dosyasının içerisine gerekli bilgileri girin.

*.env dosyası*
```console
    DB_TYPE='mysql'
    DB_HOST=''
    DB_PORT=3306
    DB_NAME='library_case'
    DB_USERNAME=''
    DB_PASS=''
```
ve aşağıdaki kod ile uygulamayı çalıştırın.

```console
    foo@bar:~$ npm i . && npm run start
```


Kodu çalıştırdıktan sonra, Postman'den http://localhost:3000 linkine istek atabilirsiniz.

Herhangi bir problemle karşılaştığınızda, <cemil.sevim@hotmail.com> adresine mail gönderebilirsiniz.

Teşekkürler.