FROM node

WORKDIR /usr/src/library_case_api

COPY . .

RUN npm install

EXPOSE 3000

CMD [ "npm", "start"]