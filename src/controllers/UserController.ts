import {JsonController, Get, Post, Body, HttpError, Param} from "routing-controllers";
import UserService from "../services/UserService";
import User from "../models/User";
import {MinLength, MaxLength, validateOrReject, IsNotEmpty} from "class-validator";
import BorrowService from "../services/BorrowService";
import BookService from "../services/BookService";
import {BookNotFoundError} from "./BookController";
import Book from "../models/Book";
import Borrow from "../models/Borrow";

export class CreateUserBody{

    @IsNotEmpty({
        message:"Ad alanı boş bırakılamaz."
    })
    @MinLength(2,{
        message:"Ad minumum 2 karakterden oluşmalı."
    })
    @MaxLength(225,{
        message:"Ad maksimum 225 karakterden oluşabilir."
    })
    public name: string;

}

export class UserResponse{
    public id: number;
    public name: string;
}

export class UserDetailResponse extends UserResponse{
    public books: object;
}

export class UserNotFoundError extends HttpError{
    constructor(){
        super(500,"Kullanıcı bulunamadı.");
    }
}

export class BorrowNotFoundError extends HttpError{
    constructor(){
        super(500,"Kitap kayıdı bulunamadı.");
    }
}

@JsonController('/users')
export default class UserController{
    constructor(
        private readonly userService: UserService,
        private readonly borrowService: BorrowService,
        private readonly bookService: BookService
    ){}

    @Post()
    public async create(@Body({required: true}) body: any): Promise<any>{
        // set body param
        var userBody = new CreateUserBody();
        userBody.name = body.name;

        // validate request body
        await validateOrReject(userBody);
        
        const user = new User();
        user.userName = userBody.name;
        
        await this.userService.createUser(user);

        return {
            message:"Kullanıcı oluşturuldu.",
            data:{
                id: user.userId,
                name: user.userName
            }
        };
    } 
    
    @Get()
    public async list(): Promise<UserResponse[]>{
        var userList: User[] = await this.userService.findUsers();

        var response: UserResponse[] = [];
        response = userList.map((user) => {return {id:user.userId, name:user.userName}});

        return response;
    }

    @Get("/:id")
    public async detail(@Param('id') id: number): Promise<UserDetailResponse>{
        var findUserById = await this.userService.findUserById(id);

        // kullanıcı yoksa hata fırlatır
        if(!findUserById){
            throw new UserNotFoundError();
        }

        const presentBorrow = await this.borrowService.findBorrowsByUserIdAndStatus(findUserById.userId,"waiting");
        const pastBorrow = await this.borrowService.findBorrowsByUserIdAndStatus(findUserById.userId,"returned");

        return {
            id:findUserById.userId,
            name:findUserById.userName,
            books:{
                past:pastBorrow.map((borrow) => {return {name: borrow.book.bookName, score: borrow.score}}),
                present:presentBorrow.map((borrow) => {return {name: borrow.book.bookName}})
            }
        };
    }

    @Post("/:userId/borrow/:bookId")
    public async borrowBook(@Param('userId') id: number, @Param('bookId') bookId: number): Promise<any>{
        var findUserById: User = await this.userService.findUserById(id);

        // kullanıcı yoksa hata fırlatır
        if(!findUserById){
            throw new UserNotFoundError();
        }
        
        var book: Book = await this.bookService.findBookById(bookId);

        // kitap yoksa hata fırlatır
        if(!book){
            throw new BookNotFoundError();
        }

        var newBorrow: Borrow = new Borrow();

        newBorrow.book = book;
        newBorrow.score = 0;
        newBorrow.status = "waiting";
        newBorrow.user = findUserById;

        await this.borrowService.createBorrow(newBorrow);

        return {
            message:"Ödünç kitap kayıdı oluşturuldu."
        };
    }
    
    @Post("/:userId/return/:bookId")
    public async returnBook(@Body({required: true}) body: any, @Param('userId') id: number, @Param('bookId') bookId: number): Promise<any>{
        var findUserById: User = await this.userService.findUserById(id);
        // kullanıcı yoksa hata fırlatır
        if(!findUserById){
            throw new UserNotFoundError();
        }
        
        var book: Book = await this.bookService.findBookById(bookId);
        // kitap yoksa hata fırlatır
        if(!book){
            throw new BookNotFoundError();
        }


        var findBorrow = await this.borrowService.findWaitingBorrow(findUserById.userId,bookId);
        // ödünç kayıdı yoksa, hata fırlatır.
        if(!findBorrow){
            throw new BorrowNotFoundError();
        }

        var score: number = parseInt(body.score);
        score = score <= 0 ? 0 : score;

        findBorrow.score = score;
        findBorrow.status = "returned";

        await this.borrowService.updateBorrow(findBorrow);

        return {
            message:"Kitap iade edildi."
        }
    }
}