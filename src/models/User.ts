import { IsNotEmpty } from 'class-validator';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn, OneToMany, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';
import Borrow from "./Borrow";

@Entity()
class User{
    @PrimaryGeneratedColumn()
    public userId: number;

    @IsNotEmpty()
    @Column()
    public userName: string;

    @CreateDateColumn()
    public createdAt: string;

    @UpdateDateColumn()
    public updatedAt: string;
}

export default User;