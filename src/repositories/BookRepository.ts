import { EntityRepository, Repository } from 'typeorm';
import Book from '../models/Book';
import Borrow from "../models/Borrow";

@EntityRepository(Book)
export default class BookRepository extends Repository<Book> {}