import { EntityRepository, Repository } from 'typeorm';
import Borrow from '../models/Borrow';

@EntityRepository(Borrow)
export default class BorrowRepository extends Repository<Borrow> {
    public async findBorrowsByUserIdAndStatus(userId: number, status: string): Promise<Borrow[]>{
        return this.find({
                relations:["book"],
                where:{
                    user:{
                        userId
                    },
                    status
                }
            });
     }

     public getAvgScoreOfBook(bookId: number): Promise<Borrow>{
        return this.createQueryBuilder()
            .select("AVG(score) as avg")
            .where("bookId = :bookId",{bookId})
            .getRawOne();
    }
}