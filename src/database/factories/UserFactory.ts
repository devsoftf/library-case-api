import * as Faker from 'faker';
import { define } from 'typeorm-seeding';

import User from '../../models/User';

define(User, (faker: typeof Faker) => {
    const user = new User();
    user.userName = faker.name.findName(); // generate new fake user name

    return user;
});